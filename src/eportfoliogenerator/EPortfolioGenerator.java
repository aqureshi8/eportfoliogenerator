package eportfoliogenerator;

import static eportfoliogenerator.StartupConstants.*;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * 
 * ePortfolioGenerator is a program that allows users to create their own eportfolio
 * to showcase themselves and their work.
 * 
 * @author Ahsan Qureshi
 */
public class EPortfolioGenerator extends Application {
    
    EPortfolioGeneratorView epgView = new EPortfolioGeneratorView();
    
    @Override
    public void start(Stage primaryStage) {
        
        primaryStage.setTitle("ePortfolio Generator");
        primaryStage.getIcons().add(new Image("file:" + PATH_ICONS + "ePortGenIcon.png"));
        epgView.startUI(primaryStage);        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
