package eportfoliogenerator;

/**
 * This class provides setup constants for initializing the application.
 * 
 * @author Ahsan Qureshi
 */

public class StartupConstants {
    
    //Path Constants
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_CSS = "eportfoliogenerator/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGeneratorStyle.css";
    public static String PATH_SITES = "./sites/";
    public static String PATH_EPORTS = "./eportfolios/";

    //CSS Class Names
    public static String CSS_FILE_TOOLBAR_CLASS = "top_toolbar";
    public static String CSS_SITE_TOOLBAR_CLASS = "site_toolbar";
    public static String CSS_COMPONENT_TOOLBAR_CLASS = "component_toolbar";
    public static String CSS_FILE_TOOLBAR_BUTTON_CLASS = "file_toolbar_button";
    public static String CSS_LEFT_TOOLBAR_BUTTON_CLASS = "left_toolbar_button";
    public static String CSS_EDITOR_TOOLBAR_BUTTON_CLASS = "edit_toolbar_button";
    public static String CSS_BOTTOM_TOOLBAR_CLASS = "bottom_toolbar";
    public static String CSS_PAGE_SELECT_CLASS = "page_select";
    public static String CSS_WORKSPACE_CLASS = "workspace";
    public static String CSS_PAGE_SELECT_VIEW_CLASS = "selected_page";
    public static String CSS_PAGE_VIEW_CLASS = "page";
    public static String CSS_BANNER_CLASS = "banner";
    public static String CSS_NAV_CLASS = "nav";
    public static String CSS_NAV_SELECT_CLASS = "nav_select";
    public static String CSS_SELECTED_COMPONENT_CLASS = "selected_component";
    public static String CSS_COMPONENT_CLASS = "component";
    public static String CSS_BANNER_IMAGE_CLASS = "banner_image";
    public static String CSS_FOOTER_CLASS = "footer";
    public static String CSS_FOOTER_TEXT_CLASS = "footer_text";
    public static String CSS_CONTENT_CLASS = "content";
    public static String CSS_PAGE_NAME_CLASS = "page_name";
    public static String CSS_DIALOG_CLASS = "dialog";
    public static String CSS_DIALOG_TEXT_CLASS = "dialog_text";
    public static String CSS_BANNER_TEXT_CLASS = "banner_text";
    public static String CSS_SELECTED_SLIDE_CLASS = "selected_slide";
    public static String CSS_SLIDE_CLASS = "slide";
    public static String CSS_HEADER_CLASS = "header";
    public static String CSS_FONT = "font";
    public static String CSS_PARAGRAPH_CLASS = "paragraph";
    public static String CSS_LIST_CLASS = "list";
    public static String CSS_IMAGE_CLASS = "image";
    public static String CSS_ALIGN = "align";
    public static String CSS_VIDEO_CLASS = "video";
    public static String CSS_SLIDESHOW_CLASS = "slideshow";
    
    //GUI Icons
    public static String ICON_NEW = "New.png";
    public static String ICON_LOAD = "Load.png";
    public static String ICON_SAVE = "Save.png";
    public static String ICON_SAVEAS = "SaveAs.png";
    public static String ICON_EXPORT = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_MOVEUP = "moveup";
    public static String ICON_ADD = "plus";
    public static String ICON_REMOVE = "minus";
    public static String ICON_MOVEDOWN = "movedown";
    public static String ICON_EDITVIEW = "EditView.png";
    public static String ICON_SITEVIEW = "SiteView.png";
    public static String ICON_LAYOUT = "Layout.png";
    public static String ICON_COLOR = "Color.png";
    public static String ICON_TEXT = "Text.png";
    public static String ICON_IMAGE = "Image.png";
    public static String ICON_SLIDESHOW = "Slideshow.png";
    public static String ICON_VIDEO = "Video.png";
    public static String ICON_REMOVECOMPONENT = "RemoveComponent.png";
    public static String ICON_EDIT = "Edit.png";
    public static String ICON_PAGE = "Page.png";
    
    //Tooltips
    public static String TOOLTIP_NEW = "Create a new ePortfolio";
    public static String TOOLTIP_LOAD = "Load a previous ePortfolio";
    public static String TOOLTIP_SAVE = "Save your current ePortfolio";
    public static String TOOLTIP_SAVEAS = "Name and save your current ePortfolio";
    public static String TOOLTIP_EXPORT = "Export.png";
    public static String TOOLTIP_EXIT = "Exit the application";
    public static String TOOLTIP_MOVEUP = "Move the current slide up";
    public static String TOOLTIP_ADD = "Add a new page";
    public static String TOOLTIP_REMOVE = "Remove a new page";
    public static String TOOLTIP_MOVEDOWN = "Move the current slide down";
    public static String TOOLTIP_EDITVIEW = "Switch to ePortfolio Editor";
    public static String TOOLTIP_SITEVIEW = "Switch to ePortfolio Site View";
    public static String TOOLTIP_LAYOUT = "Choose a layout";
    public static String TOOLTIP_COLOR = "Choose a color scheme";
    public static String TOOLTIP_TEXT = "Add a text component";
    public static String TOOLTIP_IMAGE = "Add an image component";
    public static String TOOLTIP_SLIDESHOW = "Add a slideshow component";
    public static String TOOLTIP_VIDEO = "Add a video component";
    public static String TOOLTIP_REMOVECOMPONENT = "Remove selected component";
    public static String TOOLTIP_EDIT = "Edit selected component";
    
    //Layouts
    public static String DEFAULT_LAYOUT = "Top-Left Nav";
    
    //Color schemes
    public static String DEFAULT_COLORS = "Arcade";
    
    //Fonts
    public static String DEFAULT_FONT = "Alegreya Sans";
}
