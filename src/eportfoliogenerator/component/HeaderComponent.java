/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;

/**
 *
 * @author ahsanqureshi
 */
public class HeaderComponent extends Component{
    
    private String header;
    
    public HeaderComponent(String text, String initFont) {
        
        type = "HEADER";
        header = text;
        font = initFont;
    }
    
    public void setHeader(String h) {
        header = h;
    }
    
    public String getHeader() {
        return header;
    }
}
