/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;
import javafx.scene.image.Image;

/**
 *
 * @author ahsanqureshi
 */
public class ImageComponent extends Component {
    
    private Image image;
    private String imageName;
    private String imagePath;
    private int height;
    private int width;
    private String caption;
    private String alignment;
    
    public ImageComponent(Image i, String in, String ip, int h, int w, String c, String a, String f) {
        
        type = "IMAGE";
        imageName = in;
        imagePath = ip;
        image = i;
        height = h;
        width = w;
        caption = c;
        alignment = a;
        font = f;
    }
    
    public Image getImage() {
        return image;
    }
    
    public void setImage(Image i) {
        image = i;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void setHeight(int h) {
        height = h;
    }
    
    public int getWidth() {
        return width;
    }
    
    public void setWidth(int w) {
        width = w;
    }
    
    public String getCaption() {
        return caption;
    }
    
    public void setCaption(String c) {
        caption = c;
    }
    
    public String getAlignment() {
        return alignment;
    }
    
    public void setAlignment(String a) {
        alignment = a;
    }
    
    public String getImageName() {
        return imageName;
    }
    
    public String getImagePath() {
        return imagePath;
    }
}
