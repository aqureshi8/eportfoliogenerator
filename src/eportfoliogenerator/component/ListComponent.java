/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author ahsanqureshi
 */
public class ListComponent extends Component{
    
    ArrayList<String> list = new ArrayList<String>();
    
    public ListComponent(ObservableList<String> text, String initFont) {
        
        type = "LIST";
        for(String s : text) {
            list.add(s);
        }
        font = initFont;
    }
    
    public void setList(ArrayList<String> l) {
        list = l;
    }
    
    public ArrayList<String> getList() {
        return list;
    }
    
}
