/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;

/**
 *
 * @author ahsanqureshi
 */
public class ParagraphComponent extends Component{
    
    String paragraph;
    
    public ParagraphComponent(String text, String initFont) {
        
        type = "PARAGRAPH";
        paragraph = text;
        font = initFont;
    }
    
    public void setParagraph( String p ) {
        paragraph = p;
    }
    
    public String getParagraph() {
        return paragraph;
    }
}
