/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;
import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author ahsanqureshi
 */
public class SlideShowComponent extends Component{
    
    ArrayList<String> imageNames;
    ArrayList<String> imagePaths;
    ArrayList<String> captions;
    
    public SlideShowComponent(ArrayList<String> in, ArrayList<String> ip, ArrayList<String> c, String f) {
        
        type = "SLIDESHOW";
        imageNames = in;
        imagePaths = ip;
        captions = c;
        font = f;
    }
    
    public ArrayList<String> getImageNames() {
        return imageNames;
    }
    
    public ArrayList<String> getImagePaths() {
        return imagePaths;
    }
    
    public ArrayList<String> getCaptions() {
        return captions;
    }    
    
    public void setImageNames(ArrayList<String> i) {
        imageNames = i;
    }
    
    public void setImagePaths(ArrayList<String> p) {
        imagePaths = p;
    }
    
    public void setCaptions(ArrayList<String> c) {
        captions = c;
    }
}
