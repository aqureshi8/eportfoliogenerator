/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.component;

import eportfoliogenerator.model.Component;
import javafx.scene.image.Image;

/**
 *
 * @author ahsanqureshi
 */
public class VideoComponent extends Component {
    
    private  String video;
    private int height;
    private int width;
    private String caption;
    
    public VideoComponent( String v, int h, int w, String c, String f) {
        
        type = "VIDEO";
        video = v;
        height = h;
        width = w;
        caption = c;
        font = f;
    }
    
    public String getVideo() {
        return video;
    }
    
    public void setVideo(String v) {
        video = v;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void setHeight(int h) {
        height = h;
    }
    
    public int getWidth() {
        return width;
    }
    
    public void setWidth(int w) {
        width = w;
    }
    
    public String getCaption() {
        return caption;
    }
    
    public void setCaption(String c) {
        caption = c;
    }  
}
