/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import eportfoliogenerator.component.HeaderComponent;
import eportfoliogenerator.component.ImageComponent;
import eportfoliogenerator.component.ListComponent;
import eportfoliogenerator.component.ParagraphComponent;
import eportfoliogenerator.component.SlideShowComponent;
import eportfoliogenerator.component.VideoComponent;
import eportfoliogenerator.dialogs.HeaderDialog;
import eportfoliogenerator.dialogs.ImageDialog;
import eportfoliogenerator.dialogs.ListDialog;
import eportfoliogenerator.dialogs.ParagraphDialog;
import eportfoliogenerator.dialogs.SlideshowDialog;
import eportfoliogenerator.dialogs.TextDialog;
import eportfoliogenerator.dialogs.VideoDialog;
import eportfoliogenerator.model.Slide;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;

/**
 *
 * @author Ahsan Qureshi
 */
public class ComponentController {
    
    EPortfolioGeneratorView ui;
    
    public ComponentController(EPortfolioGeneratorView u) {
     
        ui = u;
        
    }
    
    public void handleAddText() {
        
        TextDialog td = new TextDialog();
        
        String tComponent = td.getTextComponent();
        
        if(tComponent != null) {
            switch (tComponent) {
                case "Header":
                    handleAddHeader();
                    break;
                case "Paragraph":
                    handleAddParagraph();
                    break;
                case "List":
                    handleAddList();
                    break;
            }
        }
    }

    public void handleAddHeader() {
    
        HeaderDialog hd = new HeaderDialog();
        String header = hd.getText();
        if(header != null) {
            ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new HeaderComponent(header, ui.getFont()));
            ui.reloadEPortfolio();
        }
    }
    
    public void handleAddParagraph() {

        ParagraphDialog pd = new ParagraphDialog();
        if(pd.add()) {
            ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new ParagraphComponent(pd.getParagraph(),pd.getFont()));
            ui.reloadEPortfolio();
        }
    }
    
    public void handleAddList() {
        
        ListDialog l = new ListDialog();
        if(l.add()) {
            ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new ListComponent(l.getList(), l.getFont()));
            ui.reloadEPortfolio();
        }
    }
    
    public void handleAddImage() {

        ImageDialog i = new ImageDialog();
        if(i.add()) {
            try{
                ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new ImageComponent(i.getImage(), i.getImageName(), i.getImagePath(), i.getImageHeight(), i.getImageWidth(), i.getCaption(), i.getAlignment(), i.getFont()));
                ui.reloadEPortfolio();
            }
            catch(Exception e) {
                //@todo
            }
        }
    }
    
    public void handleAddSlideShow() {
        
        SlideshowDialog s = new SlideshowDialog();
        if(s.add()) {
            ArrayList<String> in = new ArrayList<String>();
            ArrayList<String> ip = new ArrayList<String>();
            ArrayList<String> c = new ArrayList<String>();
            for(Slide slide: s.getSlides()) {
                in.add(slide.getImageFileName());
                ip.add(slide.getImagePath());
                c.add(slide.getCaption());
            }
            ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new SlideShowComponent(in, ip, c, s.getFont()));
            ui.reloadEPortfolio();
        }
        
    }

    public void handleAddVideo() {
        
        VideoDialog v = new VideoDialog();
        if(v.add()) {
            try {
                ui.getEPortfolio().getPages().get(ui.getSelected()).addComponent(new VideoComponent(v.getVidPath(), v.getVidHeight(), v.getVidWidth(), v.getCaption(), v.getFont()));
                ui.reloadEPortfolio();
            }
            catch(Exception e) {
                //@todo
            }
        }
    }
}
