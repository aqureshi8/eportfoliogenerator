/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import static eportfoliogenerator.StartupConstants.PATH_EPORTS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import static eportfoliogenerator.StartupConstants.PATH_SITES;
import eportfoliogenerator.dialogs.ChangeTextDialog;
import eportfoliogenerator.file.EPortfolioFileManager;
import eportfoliogenerator.model.EPortfolioModel;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Ahsan Qureshi
 */
public class FileController {

    //Keep track of when something has not been saved
    private boolean saved;
    
    //The app UI
    EPortfolioGeneratorView ui;
    
    private EPortfolioFileManager ePortfolioIO;
    
    public FileController(EPortfolioGeneratorView u, EPortfolioFileManager e) {
       
        ui = u;
        ePortfolioIO = e;
        
    }
    
    public void handleBannerTextChangeRequest() {
        
        ChangeTextDialog  changeTextDialog = new ChangeTextDialog("Change ePortfolio Title", "Enter a title for your ePortfolio:", ui.getEPortfolio().getBannerText());
        ui.getEPortfolio().setBannerText(changeTextDialog.getNewText());
        ui.reloadEPortfolio();
    }
    
    public void handleFooterTextChangeRequest() {
        
        ChangeTextDialog changeTextDialog = new ChangeTextDialog("Change ePortfolio Footer", "Enter a footer for your ePortfolio:", ui.getEPortfolio().getFooter());
        ui.getEPortfolio().setFooter(changeTextDialog.getNewText());
        ui.reloadEPortfolio();
    }
    
    public void handleBannerImageChangeRequest() {
        
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    
            URL fileURL;
       
            try{
            fileURL = file.toURI().toURL();
            }
            catch (Exception e){
                return;
            }
            
            ui.getEPortfolio().setBannerImage(new Image(fileURL.toExternalForm()), file.getName(), file.getPath());            
           
            try{
                ui.reloadEPortfolio();
            }
            catch(Exception e) {
                System.out.println(e);
                return;
            }
            ui.reloadEPortfolio();
	}	    
	else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("File Error");
            alert.setContentText("No image file was selected.");
            alert.showAndWait();
	}
    }
    
    public void handleNewEportfolioRequest() {
        
        try {
            //Check if we need to save current work
            boolean continueToMakeNew = true;
            if(!saved) {
                //User can opt out here with a cancel
                //continueToMakeNew = promptToSave();
            }
            
            //If user really wants to make a new ePortfolio
            if(continueToMakeNew) {
                EPortfolioModel ePort = ui.getEPortfolio();
                ePort.reset();
                saved = false;
                
                ui.updateToolbarControls(saved);
                ui.setSelected(0);
                ui.reloadEPortfolio();
            }
        }
        catch (Exception e){
            //todo Error Handler
        }
    }
    
    public boolean handleSaveEPortRequest() {
        
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    EPortfolioModel ePortToSave = ui.getEPortfolio();
	    
            // SAVE IT TO A FILE
            ePortfolioIO.saveEPortfolio(ePortToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (Exception ioe) {
            System.out.println(ioe);
            //ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(ERROR_DATA_FILE_SAVING, "Error", "Message");
	    return false;
        }
    }
    
    public boolean handleSaveAsEPortRequest() {
        
        ChangeTextDialog  changeTextDialog = new ChangeTextDialog("Save As", "Enter file name:", ui.getEPortfolio().getBannerText());
        ui.getEPortfolio().setSaveAs(changeTextDialog.getNewText());
        
        return handleSaveEPortRequest();
    }
    
    public void handleLoadEPortRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                //continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (Exception ioe) {
            System.out.println(ioe);
            //ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(ERROR_DATA_FILE_LOADING, "Error", "Message");//@todo provide error message
        }
    }
    
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser eportFileChooser = new FileChooser();
        eportFileChooser.setInitialDirectory(new File(PATH_EPORTS));
        File selectedFile = eportFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		EPortfolioModel eportToLoad = ui.getEPortfolio();
                ePortfolioIO.loadEPort(eportToLoad, selectedFile.getAbsolutePath());
                ui.setEPortfolio(eportToLoad);
                ui.reloadEPortfolio();
            } catch (Exception e) {
                System.out.println(e + "HERE");
                //ErrorHandler eH = ui.getErrorHandler();
                //eH.processError(ERROR_DATA_FILE_LOADING, "Error", "File Load");// @todo
                saved = true;
                //handleNewSlideShowRequest();
            }
        }
    }
    
    public void handlePageEditButtonRequest() {
        
        if(ui.getSecondStage() != null) {
            ui.getSecondStage().close();
        }
    }
    
    public Stage handleViewSlideShowRequest() {
        //Create Stage
        Stage slideViewStage = new Stage();
        
        //Get HTML File
        File htmlFile = new File(PATH_SITES + "My Birthday/index.html");
        
        //Create a webview
        WebView wv = new WebView();
        WebEngine we = wv.getEngine();
        
        try {
            we.load(htmlFile.toURI().toURL().toString());
        }
        catch(Exception e) {}
        
        //Create Scene
        Scene slideViewScene = new Scene(wv, 1000, 714);
        
        //show stage
        slideViewStage.setScene(slideViewScene);
        slideViewStage.show();
        return slideViewStage;
    }    
}
