/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import eportfoliogenerator.dialogs.ColorSchemeDialog;
import eportfoliogenerator.dialogs.LayoutDialog;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.util.ArrayList;
import javafx.scene.control.ChoiceDialog;

/**
 *
 * @author ahsanqureshi
 */
public class LayoutColorController {
    
    EPortfolioGeneratorView ui;
    
    public LayoutColorController(EPortfolioGeneratorView u) {
     
        ui = u;
        
    }
    
    public void handleLayoutSelect() {
       
        LayoutDialog ld = new LayoutDialog(ui.getEPortfolio().getPages().get(ui.getSelected()).getLayout());
        ui.getEPortfolio().getPages().get(ui.getSelected()).setLayout(ld.getLayout());
        ui.reloadEPortfolio();
    
    }

    public void handleColorSelect() {
        
        ColorSchemeDialog csd = new ColorSchemeDialog(ui.getEPortfolio().getPages().get(ui.getSelected()).getColors());
        ui.getEPortfolio().getPages().get(ui.getSelected()).setColors(csd.getColors());
        ui.reloadEPortfolio();
        
    }
}
