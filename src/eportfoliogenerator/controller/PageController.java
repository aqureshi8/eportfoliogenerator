/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import eportfoliogenerator.dialogs.ChangeTextDialog;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.view.EPortfolioGeneratorView;

/**
 *
 * @author ahsanqureshi
 */
public class PageController {
    
    EPortfolioGeneratorView ui;
    
    public PageController (EPortfolioGeneratorView u) {
       
        ui = u;
    
    }
    
    public void handleAddPageRequest() {
        
        ui.getEPortfolio().getPages().add(new Page());
        ui.setSelected(ui.getEPortfolio().getPages().size() - 1);
        ui.reloadEPortfolio();
        ui.updatePageControls(false);
    }
    
    public void handleRemovePageRequest() {
        
        ui.getEPortfolio().getPages().remove(ui.getSelected());
        if(!(ui.getSelected() < ui.getEPortfolio().getPages().size())) {
            ui.setSelected(ui.getSelected() - 1);
        }
        if(ui.getSelected() < 0) {
            handleAddPageRequest();
        }
        ui.reloadEPortfolio();
        if(ui.getEPortfolio().getPages().size() == 1) {
            ui.updatePageControls(true);
        }
    }
    
    public void handlePageUpRequest() {
        
        if(ui.getSelected() != 0) {
            Page p = ui.getEPortfolio().getPages().get(ui.getSelected());
            ui.getEPortfolio().getPages().remove(ui.getSelected());
            ui.getEPortfolio().getPages().add(ui.getSelected() - 1, p);
            ui.setSelected(ui.getSelected() - 1);
            ui.reloadEPortfolio();
        }
        else return;
    }
    
    public void handlePageDownRequest() {
        
        if(ui.getSelected() != ui.getEPortfolio().getPages().size() - 1) {
            Page p = ui.getEPortfolio().getPages().get(ui.getSelected());
            ui.getEPortfolio().getPages().remove(ui.getSelected());
            ui.getEPortfolio().getPages().add(ui.getSelected() + 1, p);
            ui.setSelected(ui.getSelected() + 1);
            ui.reloadEPortfolio();
        }
        else return;
    }
    
    public void handleChangePageName() {
        
        ChangeTextDialog ctd = new ChangeTextDialog("Page Name Change", "Enter page name:", ui.getEPortfolio().getPages().get(ui.getSelected()).getName());
        ui.getEPortfolio().getPages().get(ui.getSelected()).setName(ctd.getNewText());
        ui.reloadEPortfolio();
    }
}