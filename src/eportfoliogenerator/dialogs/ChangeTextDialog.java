/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class ChangeTextDialog extends Stage {
    
    Scene scene;
    VBox content;
    
    Label label;
    TextField textField;
    
    HBox endButtons;
    Button okButton;
    Button cancelButton;
    
    boolean changeName;
    
    String oldN;
    
    public ChangeTextDialog(String t, String l, String n) {
        
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        label = new Label(l);
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        
        textField = new TextField(n);
                
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        initEventHandlers();
        
        content.getChildren().addAll(label, textField, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle(t);
        
        oldN = n;
    }
    
    public String getNewText() {
        
        this.showAndWait();
        
        if(changeName) {
            return textField.getText();
        }
        else {
            return oldN;
        }
    }
    
    public void initEventHandlers() {
        
        okButton.setOnAction( e -> {
            changeName = true;
            this.close();
        });
        cancelButton.setOnAction( e -> {
            this.close();
        });
    }
}
