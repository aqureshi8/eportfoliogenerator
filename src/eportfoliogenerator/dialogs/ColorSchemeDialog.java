/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class ColorSchemeDialog extends Stage {
    
    Scene scene;
    VBox content;
    
    Label label;
       
    ComboBox colorSchemes;
    
    HBox endButtons;
    Button okButton;
    Button cancelButton;
    
    boolean changeColors;
    String oldColors;
    
    public ColorSchemeDialog(String initColors) {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        label = new Label("Choose a color scheme for your ePortfolio:");
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        
        initColors();
                
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        content.getChildren().addAll(label, colorSchemes, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Color Scheme Select");
        
        changeColors = false;
        oldColors = initColors;
        
        okButton.setOnAction( e -> {
            changeColors = true;
            this.close();
        });
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
    }
 
    
    public void initColors() {
        
        colorSchemes = new ComboBox();
        colorSchemes.getItems().addAll(
                "Arcade Scheme",
                "Beach Scheme",
                "CSE 219 Scheme",
                "Executive Scheme",
                "Pastel Scheme");
        colorSchemes.setValue("Arcade Scheme");
        
    }
    
    public String getColors() {
        
        this.showAndWait();
        
        if(changeColors) {
            return (String) colorSchemes.getValue();
        }
        else return oldColors;
    }
    
}