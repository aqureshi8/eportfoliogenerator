/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.HeaderComponent;
import eportfoliogenerator.model.Component;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class HeaderDialog extends Stage {
    
     
    Scene scene;
    VBox content;
    
    Label label;
       
    TextField header;
    
    HBox endButtons;
    Button addButton;
    Button cancelButton;
    
    boolean addComponent;
    
    public HeaderDialog() {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        header = new TextField("Header");
        
        label = new Label("Enter header text:");
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
                        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(label, header, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Header Component");
        
        addComponent = false;
        
        addButton.setOnAction( e -> {
            addComponent = true;
            this.close();
        });
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
    }
    
    public HeaderDialog(Component c) {
        
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        HeaderComponent hc = (HeaderComponent) c;
        header = new TextField(hc.getHeader());
        
        label = new Label("Enter header text:");
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
                        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(label, header, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Header Component");
        
        addComponent = false;
        
        addButton.setOnAction( e -> {
            addComponent = true;
            this.close();
        });
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
    }
 
    public String getText() {
        
        this.showAndWait();
        
        if(addComponent) {
            return header.getText();
        }
        else return null;
    }
    
}
