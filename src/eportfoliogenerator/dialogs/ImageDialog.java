/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.ImageComponent;
import eportfoliogenerator.model.Component;
import java.io.File;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class ImageDialog extends Stage {
   
    Scene scene;
    VBox content;
    
    Label imgPath;
    HBox chooseAndAlign;
    Button chooseImageButton;
    VBox alignBox;
    Label align;
    ComboBox alignList;
    
    Label dimensionLabel;
    HBox dimensions;
    TextField height;
    TextField width;
    
    ComboBox componentFont;
    
    Label captionLabel;
    TextField caption;
  
    HBox endButtons;
    Button addButton;
    Button cancelButton;
    
    boolean addComponent;
    Image selectedImage;
    String imageName;
    String imagePath;
    
    public ImageDialog() {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        imgPath = new Label("");
        imgPath.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        chooseAndAlign = new HBox();
        chooseImageButton = new Button("Choose Image File");
        alignBox = new VBox();
        align = new Label("Align:");
        initAlignment();
        
        alignBox.getChildren().addAll(align, alignList);
        chooseAndAlign.getChildren().addAll(chooseImageButton, alignBox);
        
        dimensionLabel = new Label("Dimensions");
        dimensionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        dimensions = new HBox();
        height = new TextField("Height");
        width = new TextField("Width");
        dimensions.getChildren().addAll(height, width);
        
        initFonts();
        
        captionLabel = new Label("Caption:");
        captionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        caption = new TextField("Caption Here");
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(imgPath, chooseAndAlign, dimensionLabel, dimensions, componentFont, captionLabel, caption, endButtons);
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Image Component");
        this.showAndWait();
    }

    public ImageDialog(Component c) {
        
        ImageComponent ic = (ImageComponent) c;
        selectedImage = ic.getImage();
        imageName = ic.getImageName();
        imagePath = ic.getImagePath();
        
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        imgPath = new Label("Old Image");
        imgPath.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        chooseAndAlign = new HBox();
        chooseImageButton = new Button("Choose Image File");
        alignBox = new VBox();
        align = new Label("Align:");
        initAlignment();
        
        alignBox.getChildren().addAll(align, alignList);
        chooseAndAlign.getChildren().addAll(chooseImageButton, alignBox);
        
        dimensionLabel = new Label("Dimensions");
        dimensionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        dimensions = new HBox();
        height = new TextField("" + ic.getHeight());
        width = new TextField("" + ic.getWidth());
        dimensions.getChildren().addAll(height, width);
        
        initFonts();
        
        captionLabel = new Label("Caption:");
        captionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        caption = new TextField(ic.getCaption());
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Change");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(imgPath, chooseAndAlign, dimensionLabel, dimensions, componentFont, captionLabel, caption, endButtons);
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Edit Image Component");
        this.showAndWait();
    }
 
    
    public void initFonts() {
        
        componentFont = new ComboBox();
        componentFont.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        componentFont.setValue("Font: Alegreya Sans");
        
    }
    
    public void initAlignment() {
        
        alignList = new ComboBox();
        alignList.getItems().addAll(
                "Left",
                "Center",
                "Right");
        alignList.setValue("Center");
    }
    
    public void initEventHandlers() {
        
        addButton.setOnAction(e -> {
            if(imgPath.getText().length() > 0) {
                addComponent = true;
            }
            this.close();
        });
        
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
        chooseImageButton.setOnAction(e -> {
            imageSelect();
        });
    }
    
    public void imageSelect() {
        
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    
            URL fileURL;
       
            try{
                fileURL = file.toURI().toURL();
                selectedImage = new Image(fileURL.toExternalForm());
                imgPath.setText(file.getName());
                imageName = file.getName();
                imagePath = file.getAbsolutePath();
            }
            catch (Exception e){
                return;
            }
        }
    }

    public int getImageHeight() {
        return Integer.parseInt(height.getText());
    }
    
    public int getImageWidth() {
        return Integer.parseInt(width.getText());
    }
    
    public String getCaption() {
        return caption.getText();
    }
    
    public String getAlignment() {
        return alignList.getValue().toString();
    }
    
    public String getFont() {
        return componentFont.getValue().toString();
    }
    
    public boolean add() {
        return addComponent;
    }
    
    public Image getImage() {
        return selectedImage;
    }
    
    public String getImageName() {
        return imageName;
    }
    
    public String getImagePath() {
        return imagePath;
    }
}
