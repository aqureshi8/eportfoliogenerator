/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class LayoutDialog extends Stage{
    
    Scene scene;
    VBox content;
    
    Label label;
       
    ComboBox layouts;
    
    HBox endButtons;
    Button okButton;
    Button cancelButton;
    
    boolean changeLayout;
    String oldLayout;
    
    public LayoutDialog(String initLayout) {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        label = new Label("Choose a layout for your ePortfolio:");
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        
        initLayouts();
                
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        content.getChildren().addAll(label, layouts, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Layout Select");
        
        changeLayout = false;
        oldLayout = initLayout;
        
        okButton.setOnAction( e -> {
            changeLayout = true;
            this.close();
        });
        cancelButton.setOnAction( e -> {
           this.close();
        });
    }
 
    
    public void initLayouts() {
        
        layouts = new ComboBox();
        layouts.getItems().addAll(
                "Top-Left Nav",
                "Mid Nav",
                "Bottom Nav",
                "Right Nav",
                "Left Nav");
        layouts.setValue("Top-Left Nav");
        
    }
    
    public String getLayout() {
        
        this.showAndWait();

        if(changeLayout) {
            return (String) layouts.getValue();
        }
        else return oldLayout;
    }
    
}