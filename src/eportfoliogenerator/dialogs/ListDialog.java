/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.ListComponent;
import eportfoliogenerator.model.Component;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class ListDialog extends Stage {
    
    Scene scene;
    BorderPane content;
    VBox listBox;
    Label listLabel;
    ListView<String> list;
    HBox itemAddBox;
    TextField listItem;
    Button addItemButton;
    
    VBox editButtons;
    ComboBox componentFont;
    Button deleteButton;
    
    HBox endButtons;
    Button okButton;
    Button cancelButton;
    
    boolean addComponent;
  
    public ListDialog() {
                
        content = new BorderPane();        
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        listBox = new VBox();
        listBox.getStyleClass().add(CSS_DIALOG_CLASS);
        listLabel = new Label("Enter an item for your list:");
        listLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        list = new ListView<String>();
        itemAddBox = new HBox();
        listItem = new TextField();
        addItemButton = new Button("Add");
        itemAddBox.getChildren().addAll(listItem, addItemButton);
        listBox.getChildren().addAll(list, listLabel, itemAddBox);
        
        content.setLeft(listBox);
        
        editButtons = new VBox();
        editButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        editButtons.setAlignment(Pos.CENTER);
        initFonts();
        deleteButton = new Button("Delete");
        editButtons.getChildren().addAll(componentFont, deleteButton);

        content.setRight(editButtons);
        
        endButtons = new HBox();
        endButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        content.setBottom(endButtons);
        
        addComponent = false;

        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add List Component");
               
        this.showAndWait();
    }

    public ListDialog(Component c) {
        
        ListComponent lc = (ListComponent) c;
        
        content = new BorderPane();        
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        listBox = new VBox();
        listBox.getStyleClass().add(CSS_DIALOG_CLASS);
        listLabel = new Label("Enter an item for your list:");
        listLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        ObservableList<String> ols = FXCollections.observableArrayList();
        for(String s : lc.getList()) {
            ols.add(s);
        }
        list = new ListView<String>(ols);
        itemAddBox = new HBox();
        listItem = new TextField();
        addItemButton = new Button("Add");
        itemAddBox.getChildren().addAll(listItem, addItemButton);
        listBox.getChildren().addAll(list, listLabel, itemAddBox);
        
        content.setLeft(listBox);
        
        editButtons = new VBox();
        editButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        editButtons.setAlignment(Pos.CENTER);
        initFonts();
        deleteButton = new Button("Delete");
        editButtons.getChildren().addAll(componentFont, deleteButton);

        content.setRight(editButtons);
        
        endButtons = new HBox();
        endButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        content.setBottom(endButtons);
        
        addComponent = false;

        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Edit List Component");
               
        this.showAndWait();
    }
 
    
    public void initFonts() {
        
        componentFont = new ComboBox();
        componentFont.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        componentFont.setValue("Font: Alegreya Sans");
        
    }
    
    public void initEventHandlers() {
        
        okButton.setOnAction( e -> {
            if(list.getItems().isEmpty())
                addComponent = false;
            else
                addComponent = true;
            this.close();
        });
        cancelButton.setOnAction( e -> {
            addComponent = false;
            this.close();
        });
        
        addItemButton.setOnAction( e -> {
            if(listItem.getText().length() > 0) {
                list.getItems().add(listItem.getText());
            }
        });
        
        deleteButton.setOnAction( e -> {
            if( list.getSelectionModel().getSelectedIndex() >= 0) {
                list.getItems().remove(list.getSelectionModel().getSelectedIndex());
            }
        });
    }
    
    public boolean add() {
        return addComponent;
    }
    
    public ObservableList<String> getList() {
        return list.getItems();
    }

    public String getFont() {
        return componentFont.getValue().toString().substring(6);
    }
}

