/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.ParagraphComponent;
import eportfoliogenerator.model.Component;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class ParagraphDialog extends Stage{
    
    Scene scene;
    BorderPane content;
    TextArea paragraph;
    
    VBox editButtons;
    TextField urlField;
    Button hyperlinkButton;
    ComboBox componentFont;
    
    HBox endButtons;
    Button addButton;
    Button cancelButton;
    
    boolean addComponent;
    
    public ParagraphDialog() {
                
        content = new BorderPane();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        paragraph = new TextArea();
        paragraph.setPrefColumnCount(75);
        paragraph.setPrefRowCount(10);
        content.setLeft(paragraph);
        
        editButtons = new VBox();
        editButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        editButtons.setAlignment(Pos.CENTER);
        urlField = new TextField("Site Url");
        hyperlinkButton = new Button("Hyperlink");
        initFonts();
        editButtons.getChildren().addAll(urlField, hyperlinkButton, componentFont);

        content.setRight(editButtons);
        
        endButtons = new HBox();
        endButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.setBottom(endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Paragraph Component");
        addComponent = false;
        addButton.setOnAction( e -> {
            addComponent = true;
            this.close();
        });
        cancelButton.setOnAction( e -> {
            this.close();
        });
        this.showAndWait();
    }

    public ParagraphDialog(Component c) {
    
        ParagraphComponent pc = (ParagraphComponent) c;
        
        content = new BorderPane();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        paragraph = new TextArea(pc.getParagraph());
        paragraph.setPrefColumnCount(75);
        paragraph.setPrefRowCount(10);
        content.setLeft(paragraph);
        
        editButtons = new VBox();
        editButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        editButtons.setAlignment(Pos.CENTER);
        urlField = new TextField("Site Url");
        hyperlinkButton = new Button("Hyperlink");
        initFonts();
        editButtons.getChildren().addAll(urlField, hyperlinkButton, componentFont);

        content.setRight(editButtons);
        
        endButtons = new HBox();
        endButtons.getStyleClass().add(CSS_DIALOG_CLASS);
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Change");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.setBottom(endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Edit Paragraph Component");
        addComponent = false;
        addButton.setOnAction( e -> {
            addComponent = true;
            this.close();
        });
        cancelButton.setOnAction( e -> {
            this.close();
        });
        this.showAndWait();
    }
 
    
    public void initFonts() {
        
        componentFont = new ComboBox();
        componentFont.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        componentFont.setValue("Font: Alegreya Sans");
    }
    
    public boolean add() {
        return addComponent;
    }
    
    public String getParagraph() {
        return paragraph.getText();
    }
    
    public String getFont() {
        return componentFont.getValue().toString().substring(6);
    }
}
