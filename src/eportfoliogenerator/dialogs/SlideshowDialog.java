/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.SlideShowComponent;
import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.Slide;
import eportfoliogenerator.view.SlideEditView;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class SlideshowDialog extends Stage {
    
    Scene scene;
    VBox content;
    
    HBox addRemoveBox;
    Button addSlideButton;
    Button removeSlideButton;
    
    ScrollPane scroller;
    VBox slideBox;
    ArrayList<Slide> slides;
    
    ComboBox componentFont;
    
    HBox endButtons;
    Button addButton;
    Button cancelButton;
    
    int selected;
    boolean addComponent;
    
    public SlideshowDialog() {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        addRemoveBox = new HBox();
        addSlideButton = new Button("+");
        removeSlideButton = new Button("-");
        addRemoveBox.getChildren().addAll(addSlideButton, removeSlideButton);
        
        
        slideBox = new VBox();
        slides = new ArrayList<Slide>();
        scroller = new ScrollPane();
        scroller.setContent(slideBox);
        scroller.setPrefSize(250, 150);
        initFonts();
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(addRemoveBox, scroller, componentFont, endButtons);
        selected = 0;
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Slide Show Component");
        this.showAndWait();
    }

    public SlideshowDialog(Component c) {

        SlideShowComponent sc = (SlideShowComponent) c;
        
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        addRemoveBox = new HBox();
        addSlideButton = new Button("+");
        removeSlideButton = new Button("-");
        addRemoveBox.getChildren().addAll(addSlideButton, removeSlideButton);
        
        
        slideBox = new VBox();
        slides = new ArrayList<Slide>();
        int counter = 0;
        for(String i : sc.getImageNames()) {
            Slide s = new Slide(i, sc.getImagePaths().get(counter), sc.getCaptions().get(counter));
            slides.add(s);
            counter++;
        }
        reloadSlideShow();
        scroller = new ScrollPane();
        scroller.setContent(slideBox);
        scroller.setPrefSize(250, 150);
        initFonts();
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(addRemoveBox, scroller, componentFont, endButtons);
        selected = 0;
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Slide Show Component");
        this.showAndWait();
    }
 
    
    public void initFonts() {
        
        componentFont = new ComboBox();
        componentFont.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        componentFont.setValue("Font: Alegreya Sans");   
    }
    
    public void reloadSlideShow() {
	slideBox.getChildren().clear();
	for (Slide slide : slides) {
            SlideEditView slideEditor;
            
            if (slides.indexOf(slide) == selected) {
                slideEditor = new SlideEditView(slide, true);
            }
            else {
                slideEditor = new SlideEditView(slide, false);
            }
            
            initSlideEvents(slideEditor);
	    slideBox.getChildren().add(slideEditor);
	}
    }
    
    public void initEventHandlers() {
        
        addSlideButton.setOnAction( e -> {
            slides.add(new Slide());
            selected = slides.size() - 1;
            reloadSlideShow();
        });
        removeSlideButton.setOnAction(e -> {
            slides.remove(selected);
            if(slides.size() == 0) {
                slides.add(new Slide());
            }
            if(selected >= slides.size()) {
                selected--;
            }
            reloadSlideShow();
        });
        
        addButton.setOnAction(e -> {
           if(slides.size() > 0) {
               addComponent = true;
           } 
           this.close();
        });
        
        cancelButton.setOnAction(e -> {
            this.close();
        });
    }
    
    public void initSlideEvents(SlideEditView s) {
        s.setOnMouseClicked(e -> {
            selected = slides.indexOf(s.getSlide());
            reloadSlideShow();
        });
        s.getImage().setOnMouseClicked(e -> {
            selected = slides.indexOf(s.getSlide());
            handleImageSelect(s);
            reloadSlideShow();
        });
        s.getCaptionField().setOnMouseClicked(e -> {
           selected = slides.indexOf(s.getSlide());
           reloadSlideShow();
        });
        s.getCaptionField().setOnAction(e -> {
            s.getSlide().setCaption(s.getCaptionField().getText());
        });
    }
    
    public void handleImageSelect(SlideEditView view) {
        
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
            slides.get(selected).setImage(path, fileName);
            slides.get(selected).setHasImage(false);
            try{
                view.updateSlideImage();
            }
            catch(Exception e) {
                return;
            }
	}	    
	else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("File Error");
            alert.setContentText("No file selected.");
            alert.showAndWait(); // @todo provide error message for no files selected
	}
    }
    
    public boolean add() {
        return addComponent;
    }
    
    public ArrayList<Slide> getSlides() {
        return slides;
    }
    
    public String getFont() {
        return componentFont.getValue().toString().substring(6);
    }
}
