/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class TextDialog extends Stage {
    
    Scene scene;
    VBox content;
    
    Label label;
       
    ComboBox components;
    
    HBox endButtons;
    Button okButton;
    Button cancelButton;
    
    boolean addComponent;
    
    public TextDialog() {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        label = new Label("Choose a text component to add:");
        label.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        
        initTextComponents();
                
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(okButton, cancelButton);
        
        content.getChildren().addAll(label, components, endButtons);
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Text Component");
        
        addComponent = false;
        
        okButton.setOnAction( e -> {
            addComponent = true;
            this.close();
        });
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
    }
 
    
    public void initTextComponents() {
        
        components = new ComboBox();
        components.getItems().addAll(
                "Header",
                "Paragraph",
                "List");
        components.setValue("Header");
        
    }
    
    public String getTextComponent() {
        
        this.showAndWait();
        
        if(addComponent) {
            return (String) components.getValue();
        }
        else return null;
    }
}