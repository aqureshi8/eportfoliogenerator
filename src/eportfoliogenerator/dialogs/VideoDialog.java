/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.dialogs;

import static eportfoliogenerator.StartupConstants.CSS_DIALOG_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_DIALOG_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.component.VideoComponent;
import eportfoliogenerator.model.Component;
import java.io.File;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author ahsanqureshi
 */
public class VideoDialog extends Stage {
    
    Scene scene;
    VBox content;
    
    Label vidText;
    Button chooseVideoButton;
    
    Label dimensionLabel;
    HBox dimensions;
    TextField height;
    TextField width;
    
    ComboBox componentFont;
    
    Label captionLabel;
    TextField caption;
  
    HBox endButtons;
    Button addButton;
    Button cancelButton;
    
    String videoPath;
    boolean addComponent;
    
    public VideoDialog() {
                       
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        vidText = new Label("");
        vidText.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        chooseVideoButton = new Button("Choose Video File");
        
        dimensionLabel = new Label("Dimensions");
        dimensionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        dimensions = new HBox();
        height = new TextField("Height");
        width = new TextField("Width");
        dimensions.getChildren().addAll(height, width);
        
        initFonts();
        
        captionLabel = new Label("Caption:");
        captionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        caption = new TextField("Caption Here");
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Add");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(vidText, chooseVideoButton, dimensionLabel, dimensions, componentFont, captionLabel, caption, endButtons);
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Add Video Component");
        this.showAndWait();
    }

    public VideoDialog(Component c) {

        VideoComponent vc = (VideoComponent) c;
        videoPath = vc.getVideo();
        
        content = new VBox();
        content.getStylesheets().add(STYLE_SHEET_UI);
        content.getStyleClass().add(CSS_DIALOG_CLASS);
        
        vidText = new Label("Old Video");
        vidText.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        chooseVideoButton = new Button("Choose Video File");
        
        dimensionLabel = new Label("Dimensions");
        dimensionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        dimensions = new HBox();
        height = new TextField("" + vc.getHeight());
        width = new TextField("" + vc.getWidth());
        dimensions.getChildren().addAll(height, width);
        
        initFonts();
        
        captionLabel = new Label("Caption:");
        captionLabel.getStyleClass().add(CSS_DIALOG_TEXT_CLASS);
        caption = new TextField("Caption Here");
        
        endButtons = new HBox();
        endButtons.setAlignment(Pos.CENTER_RIGHT);
        addButton = new Button("Change");
        cancelButton = new Button("Cancel");
        endButtons.getChildren().addAll(addButton, cancelButton);
        
        content.getChildren().addAll(vidText, chooseVideoButton, dimensionLabel, dimensions, componentFont, captionLabel, caption, endButtons);
        
        addComponent = false;
        initEventHandlers();
        
        scene = new Scene(content);
        this.setScene(scene);
        this.setTitle("Edit Video Component");
        this.showAndWait();
    }
 
    public void initEventHandlers() {
        
        addButton.setOnAction(e -> {
            if(vidText.getText().length() > 0) {
                addComponent = true;
            }
            this.close();
        });
        
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
        chooseVideoButton.setOnAction(e -> {
            videoSelect();
        });
    }
    
    public void initFonts() {
        
        componentFont = new ComboBox();
        componentFont.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        componentFont.setValue("Font: Alegreya Sans");
        
    }
    
    public void videoSelect() {
        
        FileChooser videoFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	videoFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("FLV files (*.flv)", "*.FLV");
	videoFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = videoFileChooser.showOpenDialog(null);
	
        if (file != null) {
	           
            try{
                videoPath = file.getAbsolutePath() + "/" + file.getName();
                vidText.setText(file.getName());
            }
            catch (Exception e){
                return;
            }
        }
    }
    
    public boolean add() {
        return addComponent;
    }
    
    public String getVidPath() {
        return videoPath;
    }
    
    public int getVidHeight() {
        return Integer.parseInt(height.getText());
    }
    
    public int getVidWidth() {
        return Integer.parseInt(width.getText());
    }
    
    public String getCaption() {
        return caption.getText();
    }
    
    public String getFont() {
        return componentFont.getValue().toString().substring(6);
    }
}
