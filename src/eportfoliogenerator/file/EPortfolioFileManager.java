/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.file;

import static eportfoliogenerator.StartupConstants.PATH_EPORTS;
import static eportfoliogenerator.StartupConstants.PATH_SITES;
import eportfoliogenerator.component.HeaderComponent;
import eportfoliogenerator.component.ImageComponent;
import eportfoliogenerator.component.ListComponent;
import eportfoliogenerator.component.ParagraphComponent;
import eportfoliogenerator.component.SlideShowComponent;
import eportfoliogenerator.component.VideoComponent;
import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.EPortfolioModel;
import eportfoliogenerator.model.Page;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonWriter;

/**
 *
 * @author ahsanqureshi
 */
public class EPortfolioFileManager {
    
    public static String JSON_TITLE = "title";
    public static String JSON_PAGE_TITLE = "ptitle";
    public static String JSON_BANNER_NAME = "banner_name";
    public static String JSON_BANNER_PATH = "banner_path";
    public static String JSON_FOOTER = "footer";
    public static String JSON_PAGES = "pages";
    public static String JSON_COMPONENTS = "components";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_FILE_NAMES = "image_file_names";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_IMAGE_PATHS = "image_paths";
    public static String JSON_CAPTIONS = "captions";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String JSON_CAPTION = "caption";
    public static String JSON_TYPE = "type";
    public static String JSON_TEXT = "text";
    public static String JSON_FONT = "font";
    public static String JSON_ALIGN = "align";
    public static String JSON_HEIGHT = "height";
    public static String JSON_WIDTH = "width";
    public static String JSON_COMPONENT = "component";
    public static String JSON_COLOR = "color";
    public static String JSON_LAYOUT = "layout";
    
    public void saveEPortfolio(EPortfolioModel epmToSave) throws IOException {
        
        String jsonFilePath = PATH_EPORTS + SLASH + epmToSave.getSaveAs() + JSON_EXT;
        
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        JsonArray pagesJsonArray = makePagesJsonArray(epmToSave.getPages());
        
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, epmToSave.getBannerText())
                                    .add(JSON_BANNER_NAME, epmToSave.getBannerImageName())
                                    .add(JSON_BANNER_PATH, epmToSave.getBannerImagePath())
                                    .add(JSON_PAGES, pagesJsonArray)
                                    .add(JSON_FOOTER, epmToSave.getFooter())
                .build();

        jsonWriter.writeObject(courseJsonObject);
    }
    
    private JsonArray makePagesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonObject makePageJsonObject(Page page) {
        
        JsonArray componentsJsonArray = makeComponentsJsonArray(page.getComponents());

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_PAGE_TITLE, page.getName())
                .add(JSON_FONT, page.getFont())
                .add(JSON_COLOR, page.getColors())
                .add(JSON_LAYOUT, page.getLayout())
		.add(JSON_COMPONENT, componentsJsonArray)
		.build();
	return jso;
    }
    
    private JsonArray makeComponentsJsonArray(List<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Component component : components) {
	    JsonObject jso = makeComponentJsonObject(component);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonObject makeComponentJsonObject(Component component) {
        
        switch (component.getType()) {
            case "HEADER":
                return makeHeaderJsonObject((HeaderComponent) component);
            case "PARAGRAPH":
                return makeParagraphJsonObject((ParagraphComponent) component);
            case "LIST":
                return makeListJsonObject((ListComponent) component);
            case "IMAGE":
                return makeImageJsonObject((ImageComponent) component);
            case "VIDEO":
                return makeVideoJsonObject((VideoComponent) component);
            case "SLIDESHOW":
                return makeSlideShowJsonObject((SlideShowComponent) component);
        }
        return null;
    }
    
    private JsonObject makeHeaderJsonObject(HeaderComponent hc) {
        
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, hc.getType())
		.add(JSON_TEXT, hc.getHeader())
                .add(JSON_FONT, hc.getFont())
		.build();
	return jso;
    }

    private JsonObject makeParagraphJsonObject(ParagraphComponent pc) {

        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, pc.getType())
		.add(JSON_TEXT, pc.getParagraph())
                .add(JSON_FONT, pc.getFont())
		.build();
	return jso;
    }

    private JsonObject makeListJsonObject(ListComponent lc) {
        
        JsonArray listJsonArray = makeListJsonArray(lc.getList());
        
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, lc.getType())
		.add(JSON_TEXT, listJsonArray)
                .add(JSON_FONT, lc.getFont())
		.build();
	return jso;
    }
    
    private JsonArray makeListJsonArray(List<String> items) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : items) {
	    jsb.add(s);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }

    private JsonObject makeImageJsonObject(ImageComponent ic) {
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, ic.getType())
                .add(JSON_IMAGE_FILE_NAME, ic.getImageName())
                .add(JSON_IMAGE_PATH, ic.getImagePath())
                .add(JSON_HEIGHT, ic.getHeight())
                .add(JSON_WIDTH, ic.getWidth())
		.add(JSON_TEXT, ic.getCaption())
                .add(JSON_ALIGN, ic.getAlignment())
                .add(JSON_FONT, ic.getFont())
		.build();
	return jso;
    }

    private JsonObject makeVideoJsonObject(VideoComponent vc) {
    JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, vc.getType())
                .add(JSON_IMAGE_FILE_NAME, vc.getVideo())
                .add(JSON_HEIGHT, vc.getHeight())
                .add(JSON_WIDTH, vc.getWidth())
		.add(JSON_TEXT, vc.getCaption())
                .add(JSON_FONT, vc.getFont())
		.build();
	return jso;
    }

    private JsonObject makeSlideShowJsonObject(SlideShowComponent sc) {
        
        JsonArray imageNameJsonArray = makeImageNameJsonArray(sc.getImageNames());
        JsonArray imagePathJsonArray = makeImagePathArray(sc.getImagePaths());
        JsonArray captionsJsonArray = makeCaptionsArray(sc.getCaptions());
        
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_TYPE, sc.getType())
                .add(JSON_IMAGE_FILE_NAMES, imageNameJsonArray)
                .add(JSON_IMAGE_PATHS, imagePathJsonArray)
		.add(JSON_CAPTIONS, captionsJsonArray)
                .add(JSON_FONT, sc.getFont())
		.build();
	return jso;
    }
    
    private JsonArray makeImageNameJsonArray(List<String> items) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : items) {
	    jsb.add(s);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonArray makeImagePathArray(List<String> items) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : items) {
	    jsb.add(s);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }

    private JsonArray makeCaptionsArray(List<String> items) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : items) {
	    jsb.add(s);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    public void loadEPort(EPortfolioModel eportToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // NOW LOAD THE COURSE
        System.out.println("1");
	eportToLoad.reset();
        eportToLoad.setBannerText(json.getString(JSON_TITLE));
                               System.out.println("2");

        eportToLoad.setBannerImage(new Image("file:" + json.getString(JSON_BANNER_PATH)), json.getString(JSON_BANNER_NAME), json.getString(JSON_BANNER_PATH));
        System.out.println("this");
        eportToLoad.setFooter(json.getString(JSON_FOOTER));
        JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
        for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(i);
            System.out.println("this2");
	    eportToLoad.addPage(loadPage(pageJso));
	}
        eportToLoad.getPages().remove(0);
    } 
    
    public Page loadPage(JsonObject pageJso) {
       Page p = new Page();
       
       p.setColors(pageJso.getString(JSON_COLOR));
       p.setName(pageJso.getString(JSON_PAGE_TITLE));
       p.setFont(pageJso.getString(JSON_FONT));
       p.setLayout(pageJso.getString(JSON_LAYOUT));
       
       JsonArray jsonComponentsArray = pageJso.getJsonArray(JSON_COMPONENT);
       for (int i = 0; i < jsonComponentsArray.size(); i++) {
           JsonObject componentJso = jsonComponentsArray.getJsonObject(i);
           p.addComponent(loadComponent(componentJso));
       }
       return p;
    }
    
    public Component loadComponent(JsonObject cjso) {
        
        switch (cjso.getString(JSON_TYPE)) {
            case "HEADER":
                return loadHeader(cjso);
            case "PARAGRAPH":
                return loadParagraph(cjso);
            case "LIST":
                return loadList(cjso);
            case "IMAGE":
                return loadImage(cjso);
            case "VIDEO":
                return loadVideo(cjso);
            case "SLIDESHOW":
                return loadSlideShow(cjso);
        }
        return null;
    }
    
    public HeaderComponent loadHeader(JsonObject cjso) {
        HeaderComponent hc = new HeaderComponent(cjso.getString(JSON_TEXT), cjso.getString(JSON_FONT));
        return hc;
    }
    
    public ParagraphComponent loadParagraph(JsonObject cjso) {
        ParagraphComponent pc = new ParagraphComponent(cjso.getString(JSON_TEXT), cjso.getString(JSON_FONT));
        return pc;
    }
    
    public ListComponent loadList(JsonObject cjso) {
        
        ObservableList<String> s = FXCollections.observableArrayList();
        JsonArray jsonItemsArray = cjso.getJsonArray(JSON_TEXT);

        for(int i = 0; i < jsonItemsArray.size(); i++) {
            s.add(jsonItemsArray.getString(i));
        }
        ListComponent lc = new ListComponent(s, cjso.getString(JSON_FONT));
        return lc;
    }
    
    public ImageComponent loadImage(JsonObject cjso) {
        ImageComponent ic = new ImageComponent(new Image("file:" + cjso.getString(JSON_IMAGE_PATH)), cjso.getString(JSON_IMAGE_FILE_NAME), cjso.getString(JSON_IMAGE_PATH), cjso.getInt(JSON_HEIGHT), cjso.getInt(JSON_WIDTH), cjso.getString(JSON_TEXT), cjso.getString(JSON_ALIGN), cjso.getString(JSON_FONT));
        return ic;    
    }
    
    public VideoComponent loadVideo(JsonObject cjso) {
        VideoComponent vc = new VideoComponent(cjso.getString(JSON_IMAGE_FILE_NAME), cjso.getInt(JSON_HEIGHT), cjso.getInt(JSON_WIDTH), cjso.getString(JSON_TEXT), cjso.getString(JSON_FONT));
        return vc;    
    }
    
    public SlideShowComponent loadSlideShow(JsonObject cjso) {
        
        ArrayList<String> in,ip,cap;
        

        
        in = new ArrayList<String>();
        ip = new ArrayList<String>();
        cap = new ArrayList<String>();

        JsonArray jsonImageNameArray = cjso.getJsonArray(JSON_IMAGE_FILE_NAMES);
        JsonArray jsonImagePathArray = cjso.getJsonArray(JSON_IMAGE_PATHS);
        JsonArray jsonCaptionsArray = cjso.getJsonArray(JSON_CAPTIONS);
        

        
        for(int i = 0; i < jsonImageNameArray.size(); i++) {

            in.add(jsonImageNameArray.getString(i));

            ip.add(jsonImagePathArray.getString(i));

            cap.add(jsonCaptionsArray.getString(i));

        }

        SlideShowComponent sc = new SlideShowComponent(in, ip, cap, cjso.getString(JSON_FONT));
        return sc;
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
}
