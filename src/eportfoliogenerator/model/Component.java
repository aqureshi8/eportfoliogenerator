/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

/**
 *
 * @author ahsanqureshi
 */
public class Component {
    
    protected String type;
    protected String font;
    
    public String getType() {
        return type;
    }
    
    public String getFont() {
        return font;
    }
    
    public void setFont(String f) {
        font = f;
    }
   
}
