/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author ahsanqureshi
 */
public class EPortfolioModel {
 
    private String bannerText; //banner text of all pages
    private String saveAs;
    private String bannerImageName;
    private String bannerImagePath;
    private Image bannerImage; //page's banner image
    private String footer;
    private ArrayList<Page> pages;
    
    private static String DEFAULT_BANNER_TEXT = "Enter Banner Text";
    private static String DEFAULT_FOOTER_TEXT = "Enter Footer Text";
    private static String DEFAULT_BANNER_IMAGE = "file:" + PATH_ICONS + "DBI.jpg";

    
    public EPortfolioModel() {
       
        pages = new ArrayList<Page>();
        pages.add(new Page());
        bannerText = DEFAULT_BANNER_TEXT;
        saveAs = bannerText;
        footer = DEFAULT_FOOTER_TEXT;
        bannerImage = new Image(DEFAULT_BANNER_IMAGE);
        bannerImagePath = PATH_ICONS + "DBI.jpg";
        bannerImageName = "DBI.jpg";
    }
    
    
    public String getSaveAs() {
        return saveAs;
    }
    
    public void setSaveAs(String s) {
        saveAs = s;
    }
    
    //clears pages
    public void reset() {
        
        pages = new ArrayList<Page>();
        pages.add(new Page());
        bannerText = DEFAULT_BANNER_TEXT;
        saveAs = bannerText;
        footer = DEFAULT_FOOTER_TEXT;
        bannerImage = new Image(DEFAULT_BANNER_IMAGE);
        bannerImagePath = PATH_ICONS + "DBI.jpg";
        bannerImageName = "DBI.jpg";
    }
    
    public ArrayList<Page> getPages() { 
        return pages;  
    }
    
    public void setPages(ArrayList<Page> p) {   
        pages = p;
    }
    
    public void addPage(Page p) {
        pages.add(p);
    }
    
    public String getBannerText() {
        return bannerText;
    }
    
    public void setBannerText(String b) {
        bannerText = b;
    }
    
    public String getFooter() {
        return footer;
    }
    
    public void setFooter(String f) {
        footer = f;
    }
    
    public void setBannerImage(Image b, String imageName, String imagePath) {
                                System.out.println("23");

        bannerImage = new Image("file:" + imagePath);
                                System.out.println("3");

        bannerImageName = imageName;
                                System.out.println(imageName);

        bannerImagePath = imagePath;
                                System.out.println(imagePath);

    }
    
     public Image getBannerImage() {
        return bannerImage;   
    }
     
     public String getBannerImageName() {
         return bannerImageName;
     }
     
     public String getBannerImagePath() {
         return bannerImagePath;
     }
}
