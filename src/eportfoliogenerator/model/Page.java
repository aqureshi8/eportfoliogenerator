/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import static eportfoliogenerator.StartupConstants.DEFAULT_COLORS;
import static eportfoliogenerator.StartupConstants.DEFAULT_FONT;
import static eportfoliogenerator.StartupConstants.DEFAULT_LAYOUT;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author ahsanqureshi
 */
public class Page {
    
    private String layout; //page layout
    private String colors; //color scheme of the page
    private String font; //page font
    private String name; //name of the page
    private ArrayList<Component> components; //a list of the pages components in order
    
    //Page colors according to color scheme
    String bannerColor;
    String backgroundColor;
    String contentColor;
    String navColor;
    String navTextColor;
    String selectedNavTextColor;
    String textColor;
    String hyperlinkColor;
    
    //Default values
    private static String DEFAULT_PAGE_NAME = "ENTER PAGE NAME";
    
    public Page() {
        layout = DEFAULT_LAYOUT;
        colors = DEFAULT_COLORS;
        font = DEFAULT_FONT;
        name = DEFAULT_PAGE_NAME;
        components = new ArrayList<Component>();
        changeColors();
    }
        
    public String getBannerColor() {
        return bannerColor;
    }
    
    public String getBackgroundColor() {
        return backgroundColor;
    }
    
    public String getContentColor() {
        return contentColor;
    }
    
    public String getNavColor() {
        return navColor;
    }
    
    public String getNavTextColor() {
        return navTextColor;
    }
    
    public String getSelectedNavTextColor() {
        return selectedNavTextColor;
    }
    
    public String getTextColor() {
        return textColor;
    }
    
    public String getHyperlinkColor() {
        return hyperlinkColor;
    }
    
    public void setLayout(String l) {
        layout = l;
    }
    
    public void setColors(String c) {
        colors = c;
    }
    
    public void setFont(String f) {
        font = f;
    }
    
    public void setName(String t) {
        name = t;
    }
    
    public void setComponents(ArrayList<Component> c) {
        components = c;
    }
    
    public String getLayout() {
        return layout;
    }
    
    public String getColors() {
        return colors;
    }
    
    public String getFont() {
        return font;
    }
    
    public String getName() {
        return name;
    }
    
    public ArrayList<Component> getComponents() {
        return components;    
    }
    
    public void addComponent(Component c) {
        
        components.add(c);
        
    }
    
    public void changeColors() {
        
        if(colors.equals("Arcade")) {
            bannerColor = "black";
            backgroundColor = "blue";
            contentColor = "black";
            navColor = "black";
            selectedNavTextColor = "blue";
            navTextColor = "red";
            textColor = "yellow";
            hyperlinkColor = "green";
        }
    }
}
