/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.URL;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

/**
 *
 * @author ahsanqureshi
 */
public class Slide {
    
    String imageFileName;
    String imagePath;
    String caption;
    Image image;
    TextField captionT;
    boolean hasImage;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide() {
	imageFileName = "DBI.jpg";
	imagePath = PATH_ICONS;
        caption = "";
        captionT = new TextField(caption);
        hasImage = false;
    }
    
    public Slide(String in, String ip, String c) {
        
        hasImage = true;
        imageFileName = in;
        imagePath = ip;
        caption = c;
        captionT = new TextField(caption);
        
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption; }
    public TextField getTextField() { return captionT; }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setTextField(TextField t) {
        captionT = t;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public void setCaption(String c) {
        caption = c;
    }
    
    public Image getImage() {
        
       // if (hasImage) {
         //   return image;
        //}
        //else {
            File file = new File(imagePath + "/" + imageFileName);
        
            URL fileURL;
        
            try {
                fileURL = file.toURI().toURL();
            }
            catch(Exception e) {
                return null;
            }
            return new Image(fileURL.toExternalForm());
       // }
    }
    
    public void setHasImage(boolean s) {
        hasImage = s;
    }
}
