/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.CSS_ALIGN;
import static eportfoliogenerator.StartupConstants.CSS_FONT;
import static eportfoliogenerator.StartupConstants.CSS_HEADER_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_IMAGE_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_LIST_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_PARAGRAPH_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_VIDEO_CLASS;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import eportfoliogenerator.component.HeaderComponent;
import eportfoliogenerator.component.ImageComponent;
import eportfoliogenerator.component.ListComponent;
import eportfoliogenerator.component.ParagraphComponent;
import eportfoliogenerator.component.SlideShowComponent;
import eportfoliogenerator.component.VideoComponent;
import eportfoliogenerator.dialogs.HeaderDialog;
import eportfoliogenerator.dialogs.ImageDialog;
import eportfoliogenerator.dialogs.ListDialog;
import eportfoliogenerator.dialogs.ParagraphDialog;
import eportfoliogenerator.dialogs.SlideshowDialog;
import eportfoliogenerator.dialogs.VideoDialog;
import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.Slide;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author ahsanqureshi
 */
public class ComponentView extends VBox {
    
    //component that this component represents
    Component component;
    
    HBox buttonBox;
    Button editButton;
    Button deleteButton;
    
    EPortfolioGeneratorView ui;
    
    public ComponentView(Component c, boolean selected, EPortfolioGeneratorView uin) {
        
        component = c;
        ui = uin;
        
        editButton = new Button();
        String editimagePath = "file:" + PATH_ICONS + "Edit.png";
        
        Image editbuttonImage = new Image(editimagePath); //create image for button icon
        ImageView editbuttonImageResized = new ImageView(editbuttonImage);
        editbuttonImageResized.setFitHeight(20);
        editbuttonImageResized.setPreserveRatio(true);
        editbuttonImageResized.setSmooth(true);
        editButton.setGraphic(editbuttonImageResized); //set the image for the button
        
        editButton.setTooltip(new Tooltip("Edit This Component")); //give the button a description
        editButton.setDisable(!selected);
        
        deleteButton = new Button();
        
        String imagePath = "file:" + PATH_ICONS +"RemoveComponent.png";
        Image buttonImage = new Image(imagePath); //create image for button icon
        ImageView buttonImageResized = new ImageView(buttonImage);
        buttonImageResized.setFitHeight(20);
        buttonImageResized.setPreserveRatio(true);
        buttonImageResized.setSmooth(true);
        deleteButton.setGraphic(buttonImageResized); //set the image for the button
        
        deleteButton.setTooltip(new Tooltip("Delete This Component")); //give the button a description
        deleteButton.setDisable(!selected);
        
        buttonBox = new HBox();
        
        buttonBox.getChildren().addAll(editButton, deleteButton);
        
        
        
        initEventHandlers();
        
        switch (component.getType()) {
            case "HEADER":
                headerView();
                break;
            case "PARAGRAPH":
                paragraphView();
                break;
            case "LIST":
                listView();
                break;
            case "IMAGE":
                imageView();
                break;
            case "VIDEO":
                videoView();
                break;
            case "SLIDESHOW":
                slideshowView();
                break;
        }
    }

    private void headerView() {
        
        HeaderComponent hComp = (HeaderComponent) component;
        HBox headerBox = new HBox();
        Label header = new Label(hComp.getHeader());
        headerBox.getChildren().addAll(header, buttonBox);
        this.getChildren().addAll(headerBox);
        header.getStyleClass().add(CSS_HEADER_CLASS);
    }

    private void paragraphView() {
        
        ParagraphComponent pComp = (ParagraphComponent) component;
        Label paragraph = new Label(pComp.getParagraph());
        this.getChildren().addAll(paragraph, buttonBox);
        paragraph.getStyleClass().add(CSS_PARAGRAPH_CLASS);
    }

    private void imageView() {
        
        ImageComponent iComp = (ImageComponent) component;
        ImageView image = new ImageView(iComp.getImage());
        image.setFitHeight(iComp.getHeight());
        image.setFitWidth(iComp.getWidth());
        image.setSmooth(true);
        Label caption = new Label(iComp.getCaption());
        this.getChildren().addAll(image, caption, buttonBox);
        this.getStyleClass().add(CSS_IMAGE_CLASS);
        caption.getStyleClass().add(CSS_FONT + iComp.getFont());
        image.getStyleClass().add(CSS_ALIGN + iComp.getAlignment());
    }

    private void listView() {
        
        ListComponent lComp = (ListComponent) component;
        for( String s : lComp.getList()) {
            Label listItem = new Label("-" + s);
            listItem.getStylesheets().add(CSS_LIST_CLASS);
            this.getChildren().add(listItem);
        }
        this.getChildren().add(buttonBox);
        this.getStyleClass().add(CSS_LIST_CLASS);
        
    }

    private void videoView() {
        VideoComponent vComp = (VideoComponent) component;
        ImageView image = new ImageView(new Image("file:" + PATH_ICONS + "DVI.png"));
        image.setFitHeight(vComp.getHeight());
        image.setFitWidth(vComp.getWidth());
        image.setSmooth(true);
        Label caption = new Label(vComp.getCaption());
        this.getChildren().addAll(image, caption, buttonBox);
        this.getStyleClass().add(CSS_VIDEO_CLASS);
        caption.getStyleClass().add(CSS_FONT + vComp.getFont());
        image.getStyleClass().add(CSS_ALIGN + "Center"); 
    }

    private void slideshowView() {
        SlideShowComponent sComp = (SlideShowComponent) component;
        ImageView image = new ImageView(new Image("file:" + PATH_ICONS + "SlideShowIcon.png"));
        image.setFitHeight(300);
        image.setFitWidth(300);
        image.setSmooth(true);
        this.getChildren().addAll(image, buttonBox);
        this.getStyleClass().add(CSS_VIDEO_CLASS);
        image.getStyleClass().add(CSS_ALIGN + "Center");     }

    private void initEventHandlers() {
        
        deleteButton.setOnAction(e -> {
            ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().remove(ui.getCompSelected());
            ui.reloadEPortfolio();
        });
        
        editButton.setOnAction(e -> {
            handleEdit(ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().get(ui.getCompSelected()));
            ui.reloadEPortfolio();
        });
    }

    private void handleEdit(Component c) {
        
        switch (c.getType()) {
            case "HEADER":
                HeaderDialog hd = new HeaderDialog(c);
                String headerString = hd.getText();
                if (headerString != null) {
                    ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new HeaderComponent(headerString, ui.getFont()));
                    ui.reloadEPortfolio();
                }
                break;
            case "PARAGRAPH":
                ParagraphDialog pd = new ParagraphDialog(c);
                if(pd.add()) {
                   ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new ParagraphComponent(pd.getParagraph(),pd.getFont()));
                                        ui.reloadEPortfolio();

                }
                break;
            case "LIST":
                ListDialog ld = new ListDialog(c);
                if(ld.add()) {
                    ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new ListComponent(ld.getList(), ld.getFont()));
                                        ui.reloadEPortfolio();

                }
                break;
            case "IMAGE":
                ImageDialog id = new ImageDialog(c);
                if(id.add()) {
                    ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new ImageComponent(id.getImage(), id.getImageName(), id.getImagePath(), id.getImageHeight(), id.getImageWidth(), id.getCaption(), id.getAlignment(), id.getFont()));
                                    ui.reloadEPortfolio();

                }
                break;
            case "VIDEO":
                VideoDialog vd = new VideoDialog(c);
                if(vd.add()) {
                    ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new VideoComponent(vd.getVidPath(), vd.getVidHeight(), vd.getVidWidth(), vd.getCaption(), vd.getFont()));
                                    ui.reloadEPortfolio();

                }
                break;
            case "SLIDESHOW":
                SlideshowDialog sd = new SlideshowDialog(c);
                if(sd.add()) {
                    ArrayList<String> in = new ArrayList<String>();
                    ArrayList<String> ip = new ArrayList<String>();
                    ArrayList<String> cap = new ArrayList<String>();
                    for(Slide slide: sd.getSlides()) {
                        in.add(slide.getImageFileName());
                        ip.add(slide.getImagePath());
                        cap.add(slide.getCaption());
                    }
                    ui.getEPortfolio().getPages().get(ui.getSelected()).getComponents().set(ui.getCompSelected(), new SlideShowComponent(in, ip, cap, sd.getFont()));
                                    ui.reloadEPortfolio();

                }
                break;
        }
    }
}
