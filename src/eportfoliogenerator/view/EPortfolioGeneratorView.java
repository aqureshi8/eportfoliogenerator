/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.*;
import eportfoliogenerator.controller.ComponentController;
import eportfoliogenerator.controller.FileController;
import eportfoliogenerator.controller.LayoutColorController;
import eportfoliogenerator.controller.PageController;
import eportfoliogenerator.dialogs.ChangeTextDialog;
import eportfoliogenerator.file.EPortfolioFileManager;
import eportfoliogenerator.file.SlideShowSiteExporter;
import eportfoliogenerator.model.EPortfolioModel;
import eportfoliogenerator.model.Page;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class EPortfolioGeneratorView {
    
    //The primary stage
    Stage primaryStage;
    Scene primaryScene;
    
    //Top Toolbar and it's controls
    FlowPane fileToolbar;
    Button newButton;
    Button loadButton;
    Button saveButton;
    Button saveAsButton;
    Button exportButton;
    Button exitButton;
    Label pageName;
    
    //Left Toolbar and it's controls
    VBox pageSelect;
    FlowPane siteToolbar;
    Button moveUpButton;
    Button addPageButton;
    Button removePageButton;
    Button moveDownButton;
    
    //Bottom right toolbar, changes between page edit and website views
    FlowPane workspaceModeToolbar;
    Button pageEditButton;
    Button siteViewerButton;
    
    //Workspace and it's controls
    BorderPane workspace;
    FlowPane componentToolbar;
    Button layoutSelect;
    Button colorSelect;
    Button addText;
    Button addImage;
    Button addSlideShow;
    Button addVideo;
    Button removeComponent;
    Button editComponent;
    ComboBox fontsList;
    
    //The eportfolio we are working with
    private EPortfolioModel ePort;
    
    //Controllers
    private FileController fileController;
    private PageController pageController;
    private ComponentController componentController;
    private LayoutColorController layoutColorController;
    private EPortfolioFileManager eportManager;
    private Stage secondStage;
    
    //Currently selected page 
    private int selected;
    private int compSelected;
    
    private SlideShowSiteExporter ssse;
    
    public EPortfolioGeneratorView() {
        
        ePort = new EPortfolioModel();
        eportManager = new EPortfolioFileManager();
        selected = 0;
        compSelected = 0;
        ssse = new SlideShowSiteExporter();
    }
    
    public int getCompSelected() {
        return compSelected;
    }
    
    public void setCompSelected( int i ) {
        compSelected = i;
    }
    
    public EPortfolioModel getEPortfolio() {
        return ePort;
    }
    
    public void setEPortfolio(EPortfolioModel epm) {
        ePort = epm;
    }
    
    public void startUI(Stage pStage) {
        
        primaryStage = pStage;
        
        //Initialize toolbar along the top to interact with ePortfolio files
        initFileToolbar();
        
        //Initialize left side toolbar
        initPageSelect();
        
        //Initialize two bottoms in bottom left to switch from ePortfolio editor to site viewer
        initWorkspaceModeToolbar();
        
        //Initialize the central workspace where all eportfolio editing will be done
        initWorkspace();
        
        //Initialize the event handlers
        initEventHandlers();
        
        //Add all components to window
        initWindow();
        
        //Start a new ePortfolio
        fileController.handleNewEportfolioRequest();
    }
    
    public void initFileToolbar() {

        //Start toolbar and get its style class
        fileToolbar = new FlowPane();
        fileToolbar.setHgap(10);
        fileToolbar.getStyleClass().add(CSS_FILE_TOOLBAR_CLASS);
        
        //Initialize all of the buttons with their icon, decription, and whether or not they are disabled
        newButton = initButton(fileToolbar, ICON_NEW, TOOLTIP_NEW, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        loadButton = initButton(fileToolbar, ICON_LOAD, TOOLTIP_LOAD, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        saveButton = initButton(fileToolbar, ICON_SAVE, TOOLTIP_SAVE, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        saveAsButton = initButton(fileToolbar, ICON_SAVEAS, TOOLTIP_SAVEAS, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        exportButton = initButton(fileToolbar, ICON_EXPORT, TOOLTIP_EXPORT, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        exitButton = initButton(fileToolbar, ICON_EXIT, TOOLTIP_EXIT, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        
        //Initialize page name label
        pageName = new Label(ePort.getPages().get(selected).getName());
        pageName.getStyleClass().add(CSS_PAGE_NAME_CLASS);
        fileToolbar.getChildren().add(pageName);
        
        
    }
    
    public Button initButton(Pane toolbar, String iconImage, String tooltip, String styleClass, boolean disabled) {
       
        Button button = new Button(); //create a button
        String imagePath = "file:" + PATH_ICONS + iconImage; //path of button icon
        
        //set button image
        if (iconImage.equals("plus")) {
            button.setText("+");
        }
        else if (iconImage.equals("minus")) {
            button.setText("-");
        }
        else if (iconImage.equals("moveup")) {
            button.setText("^");
        }
        else if (iconImage.equals("movedown")) {
            button.setText("v");
        }
        else {
            Image buttonImage = new Image(imagePath); //create image for button icon
            ImageView buttonImageResized = new ImageView(buttonImage);
            buttonImageResized.setFitHeight(20);
            buttonImageResized.setPreserveRatio(true);
            buttonImageResized.setSmooth(true);
            button.setGraphic(buttonImageResized); //set the image for the button
        }
        
        button.getStyleClass().add(styleClass); //get the style class for the button
        button.setTooltip(new Tooltip(tooltip)); //give the button a description
        button.setDisable(disabled); //disable or enable the button
        toolbar.getChildren().add(button); //add teh button to the toolbar
        return button;
        
    }
    
    public void initPageSelect() {
        
        //Initialize the left side space
        pageSelect = new VBox();
        pageSelect.prefWidth(100);
        pageSelect.setSpacing(10);
        pageSelect.setPadding(new Insets(0,0,10,0));
        pageSelect.getStyleClass().add(CSS_PAGE_SELECT_CLASS);
        initSiteToolbar(); //initializes top toolbar of page select region
        pageSelect.getChildren().add(siteToolbar);
        
    }
    
    public void initSiteToolbar() {
        
        //create the toolbar that is at the top of the page select region
        siteToolbar = new FlowPane();
        siteToolbar.setPrefWrapLength(150);
        siteToolbar.getStyleClass().add(CSS_SITE_TOOLBAR_CLASS);
        
        //initialize all of the buttons with icons, tooltips, and whether or not they are disabled
        moveUpButton = initButton(siteToolbar, ICON_MOVEUP, TOOLTIP_MOVEUP, CSS_LEFT_TOOLBAR_BUTTON_CLASS, true);
        addPageButton = initButton(siteToolbar, ICON_ADD, TOOLTIP_ADD, CSS_LEFT_TOOLBAR_BUTTON_CLASS, false);
        removePageButton = initButton(siteToolbar, ICON_REMOVE, TOOLTIP_ADD, CSS_LEFT_TOOLBAR_BUTTON_CLASS, false);
        moveDownButton = initButton(siteToolbar, ICON_MOVEDOWN, TOOLTIP_MOVEDOWN, CSS_LEFT_TOOLBAR_BUTTON_CLASS, true);
        
    }
    
    public void initWorkspaceModeToolbar() {
        
        //create the toolbar at the bottom of the page
        workspaceModeToolbar = new FlowPane();
        workspaceModeToolbar.setAlignment(Pos.BOTTOM_RIGHT);
        workspaceModeToolbar.getStyleClass().add(CSS_BOTTOM_TOOLBAR_CLASS);
        
        //initialize the two buttons with icons, tooltips, and whether or not they are disabled.
        pageEditButton = initButton(workspaceModeToolbar, ICON_EDITVIEW, TOOLTIP_EDITVIEW, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
        siteViewerButton = initButton(workspaceModeToolbar, ICON_SITEVIEW, TOOLTIP_SITEVIEW, CSS_FILE_TOOLBAR_BUTTON_CLASS, false);
    }
    
    public void initWorkspace() {
        
        workspace = new BorderPane();
        workspace.getStyleClass().add(CSS_WORKSPACE_CLASS + ePort.getPages().get(selected).getBackgroundColor());
        initComponentToolbar();
        workspace.setTop(componentToolbar);
    }
    
    public void initComponentToolbar() {
        
        componentToolbar = new FlowPane();
        componentToolbar.getStyleClass().add(CSS_COMPONENT_TOOLBAR_CLASS);
        
        layoutSelect = initButton(componentToolbar, ICON_LAYOUT, TOOLTIP_LAYOUT, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        colorSelect = initButton(componentToolbar, ICON_COLOR, TOOLTIP_COLOR, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        addText = initButton(componentToolbar, ICON_TEXT, TOOLTIP_TEXT, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        addImage = initButton(componentToolbar, ICON_IMAGE, TOOLTIP_IMAGE, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        addVideo = initButton(componentToolbar, ICON_VIDEO, TOOLTIP_VIDEO, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        addSlideShow = initButton(componentToolbar, ICON_SLIDESHOW, TOOLTIP_SLIDESHOW, CSS_EDITOR_TOOLBAR_BUTTON_CLASS, false);
        initFonts(componentToolbar);
    }
    
    public void initFonts(Pane toolbar) {
        
        fontsList = new ComboBox();
        fontsList.getItems().addAll(
                "Font: Alegreya Sans",
                "Font: Corben",
                "Font: Crafty Girls",
                "Font: Libre Baskerville",
                "Font: Nova Oval",
                "Font: Nunito",
                "Font: PressStart2P",
                "Font: Tangerine");
        fontsList.setValue("Font: Alegreya Sans");
        toolbar.getChildren().add(fontsList);
        
    }
    
    public void initEventHandlers() {
        fileController = new FileController(this, eportManager);    
        pageController = new PageController(this);
        layoutColorController = new LayoutColorController(this);
        componentController = new ComponentController(this);
        //todo
        newButton.setOnAction(e -> {
            fileController.handleNewEportfolioRequest();
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveEPortRequest();
        });
        saveAsButton.setOnAction(e -> {
            fileController.handleSaveAsEPortRequest();
        });
        loadButton.setOnAction( e -> {
           fileController.handleLoadEPortRequest();
        });
        siteViewerButton.setOnAction(e -> {
           secondStage = fileController.handleViewSlideShowRequest();
        });
        pageEditButton.setOnAction(e -> {
           fileController.handlePageEditButtonRequest(); 
        });
        addPageButton.setOnAction(e -> {
           pageController.handleAddPageRequest(); 
        });
        removePageButton.setOnAction( e -> {
           pageController.handleRemovePageRequest();
        });
        moveUpButton.setOnAction( e -> {
            pageController.handlePageUpRequest();
        });
        moveDownButton.setOnAction( e -> {
            pageController.handlePageDownRequest();
        });
        pageName.setOnMouseClicked( e -> {
            pageController.handleChangePageName();
        });
        layoutSelect.setOnAction(e -> {
            layoutColorController.handleLayoutSelect();
        });
        colorSelect.setOnAction(e -> {
            layoutColorController.handleColorSelect();
        });
        addText.setOnAction(e -> {
            componentController.handleAddText();
        });
        exportButton.setOnAction(e -> {
            try{
                ssse.exportSite(ePort);
            }
            catch(Exception x){}
        });
        addImage.setOnAction(e -> {
            componentController.handleAddImage();
        });
        addSlideShow.setOnAction(e -> {
            componentController.handleAddSlideShow();
        });
        addVideo.setOnAction(e -> {
            componentController.handleAddVideo();
        });
    }
    
    public void initWindow() {
        
        // GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        
        //Create a borderpane for the app
        BorderPane ePortGenPane = new BorderPane();
        ePortGenPane.setTop(fileToolbar);
        ePortGenPane.setBottom(workspaceModeToolbar);
        ePortGenPane.setLeft(pageSelect);
        ePortGenPane.setCenter(workspace);
        
        //Create the scene
        primaryScene = new Scene(ePortGenPane);
        
        //Add stylesheet and add scene to stage
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryScene.getStylesheets().add("http://fonts.googleapis.com/css?family=Press+Start+2P");
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    public void updateToolbarControls(boolean save) {
        //todo
    }
    
    public void reloadEPortfolio() {
        
        reloadPages();
        loadPageToEdit();
        
    }
    
    public void loadPageToEdit() {
       
        //load the new page to edit
        PageEditView pageEditView = new PageEditView(ePort.getPages().get(selected), this);
        workspace.setCenter(pageEditView);
    }
    
    public void reloadPages() {
            
            pageSelect.getChildren().clear();
            pageSelect.getChildren().add(siteToolbar);
            int i = 0;
            for(Page p: ePort.getPages()) {
                PageView pageView;
                if (i == selected) {
                    pageView = new PageView(p, true);
                    pageView.setOnMouseClicked( e -> {
                        selected = ePort.getPages().indexOf(pageView.getPage());
                        reloadEPortfolio();
                    });
                }
                else {
                    pageView = new PageView(p, false);
                    pageView.setOnMouseClicked( e -> {
                        selected = ePort.getPages().indexOf(pageView.getPage());
                        reloadEPortfolio();
                    });
                }
                pageSelect.getChildren().add(pageView);
                i++;
            }
    }
    
    public void setSelected(int i) {
        
        selected = i;
    }
    
    public int getSelected() {
        
        return selected;
    }
    
    public void updatePageControls(boolean multiplePages) {
        
        moveUpButton.setDisable(multiplePages);
        moveDownButton.setDisable(multiplePages);
    }
    
    public FileController getFileController() {
        return fileController;
    }
    
    public String getFont() {
        return fontsList.getValue().toString().substring(6);
    }
    
    public Stage getWindow() {
	return primaryStage;
    }
    
    public Stage getSecondStage() {
        return secondStage;
    }
}
