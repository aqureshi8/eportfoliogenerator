/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.CSS_BANNER_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_BANNER_IMAGE_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_BANNER_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_COMPONENT_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_FOOTER_TEXT_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_NAV_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_NAV_SELECT_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_SELECTED_COMPONENT_CLASS;
import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.EPortfolioModel;
import eportfoliogenerator.model.Page;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ahsan Qureshi
 */
public class PageEditView extends BorderPane{
    
    //EPortfolio needed for navbar
    EPortfolioGeneratorView ePort;
    
    
    //The page this component edits
    Page page;
        
    HBox banner;
    Label bannerText;
    ImageView bannerImage;
    HBox navBar;
    ScrollPane sp;
    VBox content;
    HBox footer;
    Label footerText;
    
    public PageEditView(Page p, EPortfolioGeneratorView epm) {
        
        page = p;
        ePort = epm;
        
        banner = new HBox();
        banner.getStyleClass().add(CSS_BANNER_CLASS + page.getBannerColor());
        banner.setAlignment(Pos.CENTER);
        bannerText = new Label(ePort.getEPortfolio().getBannerText());
        bannerText.setOnMouseClicked( e -> {
            ePort.getFileController().handleBannerTextChangeRequest();
        });
        bannerText.getStyleClass().add(CSS_BANNER_TEXT_CLASS);
        bannerImage = new ImageView(ePort.getEPortfolio().getBannerImage());
        bannerImage.setOnMouseClicked( e -> {
            ePort.getFileController().handleBannerImageChangeRequest();
        });
        bannerImage.setFitHeight(70);
        bannerImage.setPreserveRatio(true);
        bannerImage.setSmooth(true);
        bannerImage.getStyleClass().add(CSS_BANNER_IMAGE_CLASS);
        banner.getChildren().addAll(bannerText, bannerImage);
        
        sp = new ScrollPane();
        content = new VBox();
        
        sp.setContent(content);
        sp.setFitToWidth(true);
        
        
        navBar = new HBox();
        navBar.getStyleClass().add(CSS_NAV_CLASS + page.getNavColor() + page.getNavTextColor());
        navBar.setAlignment(Pos.CENTER);
        for(Page pg : ePort.getEPortfolio().getPages()) {
            Label name = new Label(pg.getName());
            if(page == pg) {
                name.getStyleClass().add(CSS_NAV_SELECT_CLASS + page.getSelectedNavTextColor());
            }
            else
                name.getStyleClass().add(CSS_NAV_CLASS);
            navBar.getChildren().add(name);
        }
        
        content.getChildren().add(navBar);
        int counter = 0;
        for(Component component: page.getComponents()) {
            ComponentView componentView = new ComponentView(component, counter == ePort.getCompSelected(),ePort);
            if(counter == ePort.getCompSelected()) {
                componentView.getStyleClass().add(CSS_SELECTED_COMPONENT_CLASS);
            }
            else {
                componentView.getStyleClass().add(CSS_COMPONENT_CLASS);
            }
            componentView.setOnMouseClicked(e -> {
                ePort.setCompSelected(page.getComponents().indexOf(component));
                ePort.reloadEPortfolio();
            });
            content.getChildren().add(componentView);
            counter++;
        }
        
        footer = new HBox();
        footer.setAlignment(Pos.CENTER);
        footerText = new Label(ePort.getEPortfolio().getFooter());
        footerText.getStyleClass().add(CSS_FOOTER_TEXT_CLASS);
        footerText.setOnMouseClicked( e -> {
            ePort.getFileController().handleFooterTextChangeRequest();
        });
        footer.getChildren().add(footerText);
        
        this.setTop(banner);
        this.setBottom(footer);
        this.setCenter(sp);
    }
}
