/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.CSS_PAGE_SELECT_VIEW_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_PAGE_VIEW_CLASS;
import static eportfoliogenerator.StartupConstants.ICON_PAGE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import eportfoliogenerator.model.Page;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author ahsanqureshi
 */
public class PageView extends VBox {
    
    //The page that this component represents
    Page page;
    
    Label pageTitle;
    ImageView image;
        
    public PageView(Page initPage, boolean isSelected) {
        
        if(isSelected) {
            this.getStyleClass().add(CSS_PAGE_SELECT_VIEW_CLASS);
        }
        else {
            this.getStyleClass().add(CSS_PAGE_VIEW_CLASS);
        }
        
        page = initPage;
        pageTitle = new Label(page.getName());
        Image i = new Image("file:" + PATH_ICONS + ICON_PAGE);
        image = new ImageView(i);
        image.setFitWidth(75);
        image.setPreserveRatio(true);
        image.setSmooth(true);
        this.getChildren().add(pageTitle);
        this.getChildren().add(image);
        this.setAlignment(Pos.CENTER);
    }
    
    public Page getPage() {
        return page;
    }
    
}
