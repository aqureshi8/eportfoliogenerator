/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.CSS_SELECTED_SLIDE_CLASS;
import static eportfoliogenerator.StartupConstants.CSS_SLIDE_CLASS;
import eportfoliogenerator.model.Slide;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author ahsanqureshi
 */
public class SlideEditView extends HBox{
    
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    boolean eWasThrown;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
   public SlideEditView(Slide initSlide, Boolean isSelected) {
	
        //Get slide show model to add selected slide
        eWasThrown = false;
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	if(isSelected) {
            this.getStyleClass().add(CSS_SELECTED_SLIDE_CLASS);
        }
        else {
            this.getStyleClass().add(CSS_SLIDE_CLASS);
        }
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
        updateSlideImage();
        
        
	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	captionLabel = new Label("Caption:");
	captionTextField = new TextField();
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
      
        captionTextField.setOnAction(e -> {
            slide.setCaption(captionTextField.getText());
        });
    }
    
    public Boolean eThrown() {
        return eWasThrown;
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + "/" + slide.getImageFileName();
	File file = new File(imagePath);
	    // GET AND SET THE IMAGE
        URL fileURL;
        try{
            fileURL = file.toURI().toURL();
        }
        catch (Exception e){
            System.out.println(e);
            return;
        }
	    Image slideImage = slide.getImage();
                    
            imageSelectionView.setImage(slideImage);
	    
            
	    // AND RESIZE IT
	    double scaledWidth = 50;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);       
    }
    
    public Slide getSlide() {
        return slide;
    }
    
    public ImageView getImage() {
        return imageSelectionView;
    }
    
    public TextField getCaptionField() {
        return captionTextField;
    }
}
